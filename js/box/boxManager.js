// get size of object
Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

var BoxManager = (function(){

	var instance = null

	BoxManagerPrivate = function(){

		this.init = function() {
			this.boxes = {}
			this.boxIndex = 1
			this.stepIndex = 0


			if (Object.size(window.boxes) == 0){
				this.boxes['box-0-1'] = new Box(0, 1)
				this.boxes['box-0-5'] = new BoxBackward(0, 5)
				this.boxes['box-1-2'] = new Box(1, 2)
				this.boxes['box-1-4'] = new BoxBackward(1, 4)
				this.boxes['box-2-2'] = new Box(2, 2)
				this.boxes['box-2-4'] = new BoxBackward(2, 4)
				// this.boxes['box-0-6'] = new BoxForward(0, 6)
				// this.boxes['box-1-2'] = new Box(1, 2)
			}

			// init boxes
			for ( num in this.boxes ){
				box = this.boxes[num]
				box.init()
				box.on('box:forward', this.onForwardAction.bind(this))
				box.on('box:backward', this.onBackwardAction.bind(this))
			}
		}

		this.onForwardAction = function(value){
			console.log('forward', value)
			StepManager.get().setStepForward(value)
		}

		this.onBackwardAction = function(value){
			console.log('backward', value)
			StepManager.get().setStepBackward(value)
		}

		this.setStepAndBox = function(step, box) {
			var boxName = "box-"+ step + "-" + box
			console.log(boxName)

			if (this.boxes[boxName]) {
				this.boxes[boxName].open()
			};
		}

	}

	return {
		get: function(){
			if (instance == null) instance = new BoxManagerPrivate()
			return instance
		}
	}



})()