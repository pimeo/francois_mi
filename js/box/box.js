// ---- INHERITANCE
function extend(Child, Parent) {
  Child.prototype = inherit(Parent.prototype)
  Child.prototype.constructor = Child
  Child.parent = Parent.prototype
}
function inherit(proto) {
  function F() {}
  F.prototype = proto
  return new F
}
// ---- / INHERITANCE

function Box(stepId, boxId) {
	this.stepId = stepId
	this.boxId = boxId
	this.container = "box-"+this.stepId+"-"+this.boxId
	this.$btnClose = $("#"+this.container+" [data-dialog-close]")
	this.$overlay = $("#"+this.container+" .dialog__overlay")

	this.init = function(){
		this.dialog = new DialogFx(document.getElementById(this.container))
	}


}

Emitter(Box.prototype);

Box.prototype.open = function() {
	this.dialog.toggle();
	this.$btnClose.on('click', this.close.bind(this))
	this.$overlay.on('click', this.close.bind(this))
	console.log("open",this.stepId,this.boxId)
}

Box.prototype.close = function() {
	this.dialog.toggle();
	this.$btnClose.unbind('click')
	this.$overlay.unbind('click')
}


