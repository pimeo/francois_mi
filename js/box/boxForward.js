function BoxForward(stepId, boxId){
  BoxForward.parent.constructor.apply(this, [stepId, boxId]);
}

// inheritance
extend(BoxForward, Box);

// override methods
BoxForward.prototype.close = function(){
  BoxForward.parent.close.apply(this);
  this.emit('box:forward', 1)
}

