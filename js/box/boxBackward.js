function BoxBackward(stepId, boxId){
  BoxBackward.parent.constructor.apply(this, [stepId, boxId]);
}

// inheritance
extend(BoxBackward, Box);

// override methods
BoxBackward.prototype.close = function(){
  BoxBackward.parent.close.apply(this);
  this.emit('box:backward', -2)
}

