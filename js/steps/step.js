function Step(stepId) {
	this.stepId = stepId
	this.boxes  = 6
	this.boxIndex = 0

	this.setBoxIndex = function(index){


    if (index > 0){
      // forward

      if (index + this.boxIndex > this.boxes){
        var diff = (index + this.boxIndex) - this.boxes
        if (stepId != 4 ){
          window.setTimeout(function(){
            this.emit('step:next', diff)
          }.bind(this), 200)
        }else{
          this.emit('step:end', diff)
        }
      }else{
        this.forward(index);
      }

    }else{
      // backward
      if(this.boxIndex + index < 0){
        var diff = this.boxIndex + index;
        if(stepId == 0) return
        this.emit('step:prev', diff);
      }else{
        this.backward(index);
      }

    }
	}

  this.forward = function(boxValue){

    this.boxIndex += boxValue

    // --- ACTION NEXT CASE
    this.animateMascotte()

    this.animateBoxes()

    this.emit('mascote:move', boxValue)

  }

  this.backward = function(boxValue){
    // beware : negative value
    this.boxIndex += boxValue
  }

	this.animateMascotte = function(){
	}

	this.animateBoxes = function(){
		/*switch (this.boxIndex) {
        	case 1: 
        	var dlg = new DialogFx(document.getElementById('Box1'));
			dlg.toggle();
        	break;
        	case 2: 
        	var dlg = new DialogFx(document.getElementById('Box2'));
			dlg.toggle();
        	break;	
        } */

        /*for (var i = 0; this.boxIndex == i; i++) {
        	console.log(i)
        	var dlg = new DialogFx(document.getElementById('Box'+ i));
			dlg.toggle();
        };*/
		
	}

}

Emitter(Step.prototype);