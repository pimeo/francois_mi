// get size of object
Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

var StepManager = (function(){

	var instance = null

	StepManagerPrivate = function(){

		// Creation de nos etapes
		this.steps = {}

		this.stepIndex = 1

		// etape courante
		this.currentStep = 0
		this.finishedSteps = false

		this.stepsNames = Array('one', 'two', 'three', 'four')

		this.init = function(){

			if (Object.size(window.steps) == 0){
				this.steps['one'] = new Step(this.stepIndex++)
				this.steps['two'] = new Step(this.stepIndex++)
				this.steps['three'] = new Step(this.stepIndex++)
				this.steps['four'] = new Step(this.stepIndex++)
			}

			for ( num in this.steps ){
				step = this.steps[num];
				
				// forwards
				step.on('step:next', function(diffDiceValue){
					this.setNextStep(diffDiceValue)
				}.bind(this));

				// backward
				step.on('step:prev', function(diffDiceValue){
					this.setPrevStep(diffDiceValue)
				}.bind(this));

			}

			this.steps['four'].on('step:end', this.endSteps.bind(this));

		}

		this.diceLaunched = function(diceValue){
			if (this.finishedSteps) return

			var stepName = this.stepsNames[this.currentStep]
			var step = this.steps[stepName]
			step.setBoxIndex(diceValue)
			console.log(step);
			BoxManager.get().setStepAndBox(this.currentStep, step.boxIndex)

		}

		this.setNextStep = function(diffDiceValue){
			console.log('next step')
			this.currentStep += 1
			this.animateStep()
			this.diceLaunched(diffDiceValue)
		}


		this.setPrevStep = function(diffDiceValue){
			this.current -= 1
			this.animateStep()
			this.diceLaunched(diffDiceValue)
		}


		// ATTENTION BUG CLASSE NON TROUVEE
		this.animateStep = function(){
			var boxClassName = (this.currentStep)*6
			console.log(".case"+boxClassName, this.currentStep)
			$('html, body').animate({
	        scrollLeft: $(".case"+boxClassName).offset().left
	    }, 2000);
		}

		this.setStepBackward = function(value){
			this.diceLaunched(value)
		}

		this.setStepForward = function(value){
			this.diceLaunched(value)
		}

		this.endSteps = function(){
			console.log('end step');
			this.finishedSteps = true

			// off events
			for ( num in this.steps ){
				step = this.steps[num];
				step.off('step:next', function(diffDiceValue){
					this.setNextStep(diffDiceValue)
				}.bind(this));
			}
			this.steps['four'].off('step:end', this.endSteps.bind(this));

		}

	}

	// amazing events
	Emitter(StepManagerPrivate.prototype);


	return {
		get: function(){
			if (instance == null) instance = new StepManagerPrivate()
			return instance
		}
	}



})()