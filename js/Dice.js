DiceStates = {
	INIT: "INIT",
	START: "START",
	STOP: "STOP"
}

function Dice(){

	this.$diceContainer = $('.de')
	this.$btn = this.$diceContainer.find('#result')

	this.state = DiceStates.INIT


	this.bindEvents = function(){
		this.$btn.bind('click', this.onDiceClicked.bind(this));
	}

	this.onDiceClicked = function(){
		// next behavior of dice stat
		switch(this.state){
			case DiceStates.INIT:
				this.start()
				this.state = DiceStates.START
				break;
			case DiceStates.START: 
				this.stop()
				this.state = DiceStates.STOP
				break;
			case DiceStates.STOP:
				this.start()
				this.state = DiceStates.START
				break;
			default: 
				console.error('ERROR DICE EVENT')
		}
	}

	// lancement animation des
	this.start = function(){
		this.$diceContainer.addClass('active');
	}

	// arret lancement des
	this.stop = function(){
		value = Math.ceil(3*Math.random())
		console.log('DICE', value)
		this.$diceContainer.removeClass('active');
		this.emit('dice', value)
		
	}

}

Emitter(Dice.prototype)