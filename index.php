<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Jeu de l'oie</title>
	<link rel="stylesheet" href="stylesheets/style.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/stellar.js/0.6.2/jquery.stellar.min.js"></script>
	<script src="js/Emitter.js"></script>
	<script src="js/dice.js"></script>

	<script src="js/box/box.js"></script>
	<script src="js/box/boxBackward.js"></script>
	<script src="js/box/boxForward.js"></script>
	<script src="js/box/boxManager.js"></script>
	<script src="js/steps/step.js"></script>
	<script src="js/steps/stepManager.js"></script>
	<script src="js/main.js"></script>

	<script src="js/dialog/modernizr.custom.js"></script>
	<script src="js/dialog/classie.js"></script>
	<script src="js/dialog/dialogFx.js"></script>

</head>
<body>
	<div id="container">

		<div class="loader"></div>

		<!-- <div id="main-first">
			<a class="scrollTo" href="#main">Page1</a>
		</div> -->

		<div id="intro">
			<div class="center">
				<h1>Jeu de bois</h1>

				<p>
					Nous vous proposons un jeu pour comprendre comment le bois des forêts correctement géré, transporté, stocké 
					et brûlé peut remplacer avantageusement d'autres ressources non renouvelables (comme le charbon ou le pétrole) 
					pour chauffer nos maisons.
					Le principe du jeu s'inspire du classique jeu de l'oie, avec comme objectif celui de franchir les cases numérotées 
					de 1 à 28 pour atteindre la case « ARRIVÉE ». Au fil du jeu, le joueur acquière les bonnes et les mauvaises façons 
					d'exploiter le bois pour produire de l'énergie.
				</p>

				<div class="play">
					<a href="javascript:void(0);">Let's Play</a>
				</div>
				<div class="mascotte">
					
				</div>
				<div class="bulle-intro"></div>
			</div>
		</div>

		<section id="main">
			<div id="bg">
				<div class="mountain">
					<div class="mountain1" data-stellar-ratio="0.7" data-stellar-horizontal-offset="0"></div>
					<div class="mountain2" data-stellar-ratio="0.75" data-stellar-horizontal-offset="0"></div>
					<div class="mountain3" data-stellar-ratio="0.80" data-stellar-horizontal-offset="0"></div>
				</div>
				<div class="three">
					<div class="three1" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
					<div class="squirel" data-stellar-ratio="1.20" data-stellar-horizontal-offset="0"></div>
					<div class="three2" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
					<div class="three3" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
					<div class="three4" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
					<div class="three5" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
				</div>
				<div class="cloud1" data-stellar-ratio="1.50" data-stellar-horizontal-offset="0"></div>
				<div class="cloud2" data-stellar-ratio="1.55" data-stellar-horizontal-offset="0"></div>
				<div class="bg-forets" data-stellar-ratio="0.65" data-stellar-horizontal-offset="0"></div>
				<div class="bg-footer" data-stellar-ratio="1" data-stellar-horizontal-offset="0"></div>
				<div class="ciel" data-stellar-ratio="1.2" data-stellar-horizontal-offset="0"></div>
			</div>
			<div id="mascotte-main"></div>
			<div id="box-0-1" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> ok 0 - 1</h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>
			<div id="box-0-5" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> reculer 0 - 5</h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>
			<div id="box-1-2" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> ok 1 - 2</h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>
			<div id="box-1-4" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> reculer 1 - 4 </h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>
			<div id="box-2-2" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> ok 2 - 2</h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>
			<div id="box-2-4" class="dialog">
				<div class="dialog__overlay"></div>
				<div class="dialog__content">
					<h2><strong>Box</strong> reculer 2-4</h2>
					<div><button class="action" data-dialog-close>Close</button></div>
				</div>
			</div>

			<div id="plateau">
				<ul>
					<li class="case1"><span></span></li>
					<li class="case2"><span></span></li>
					<li class="case3"><span></span></li>
					<li class="case4"><span></span></li>
					<li class="case5"><span></span></li>
					<li class="case6"><span></span></li>
					
					<li class="case7"><span></span></li>
					<li class="case8"><span></span></li>
					<li class="case9"><span></span></li>
					<li class="case10"><span></span></li>
					<li class="case11"><span></span></li>
					<li class="case12"><span></span></li>
					
					<li class="case13"><span></span></li>
					<li class="case14"><span></span></li>
					<li class="case15"><span></span></li>
					<li class="case16"><span></span></li>
					<li class="case17"><span></span></li>
					<li class="case18"><span></span></li>


				</ul>
			</div>
		</section>
		<section id="fixe">
				<nav>
				</nav>
				<h1>Où est la ressource ?</h1>
				<div class="de">
					<div>
						<div id="result" class="img"></div>
					</div>
				</div>
		</section>
	</div>
<script>
	$.stellar({
		verticalScrolling:false,
		positionProperty: 'position'
	});
	$.stellar.positionProperty.position = {
	  setLeft: function($element, newLeft, originalLeft) {
	    $element.css('left', newLeft);
	  }
	}

	/*$(document).ready(function() {
		$('.scrollTo').click( function() { // Au clic sur un élément
			var page = $(this).attr('href'); // Page cible
			var speed = 750; // Durée de l'animation (en ms)
			$('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
			return false;
		});
	});*/

	$(window).load(function() {
		$(".loader").fadeOut("1000");
	})

	$(".play").click(function() {
		$("#intro").fadeOut("1000");
	})

	
</script>
</body>
</html>